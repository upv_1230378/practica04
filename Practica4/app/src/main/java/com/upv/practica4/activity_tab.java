package com.upv.practica4;


import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.widget.TextView;


public class activity_tab extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tab2);

        Intent intent = getIntent();
        TextView textView = new TextView(this);
        TextView dos = (TextView) findViewById(R.id.resultadotab2);
        dos.setText(getIntent().getExtras().getString("dos"));
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu items for use in the action bar
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_activity_tab2, menu);
        return super.onCreateOptionsMenu(menu);
    }


}
